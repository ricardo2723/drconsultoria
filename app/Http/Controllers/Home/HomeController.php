<?php

namespace App\Http\Controllers\Home;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\RequestSendConsulta;
use Illuminate\Support\Facades\Mail;

class HomeController extends Controller
{
    public function index()
    {
        return view('home.index');
    }

    public function sendConsulta(RequestSendConsulta $request)
    {
        Mail::send('emails.email-contacts', [
            'msg' => $request->message
        ], function ($email) use($request) {
            $email->from($request->email, $request->name);
            $email->to('consultas@drconsultoriajuridica.com')->subject('Consultas OnLine');
        });

        return redirect()->back()->with('flash_message')
    }

    public function store(Request $request)
    {

    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
