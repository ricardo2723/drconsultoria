<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RequestSendConsulta extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required|email',
            'subject' => 'required',
            'message' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'O nome é Obrigatório',
            'email.required' => 'O Email é Obrigatório',
            'email.email' => 'O Email é não e válido',
            'subject.required' => 'O Assunto é Obrigatório',
            'message.required' => 'A Mensagem é Obrigatória'
        ];
    }
}
