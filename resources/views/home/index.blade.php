@extends('templates.master')


@section('content')

<style>
.invalid-feedback{
    color: red;
}
</style>

<aside id="colorlib-hero" class="js-fullheight">
    <div class="flexslider js-fullheight">
        <ul class="slides">
           <li style="background-image: url(images/img_bg_1.jpg);">
               <div class="overlay-gradient"></div>
               <div class="container">
                   <div class="row">
                       <div class="col-md-8 col-md-offset-2 text-center js-fullheight slider-text">
                           <div class="slider-text-inner">
                               <h1>Experiência em Direito Empresarial</h1>
                                <h2>Sua Solução Legal Começa Aqui!</h2>
                                <p><a class="btn btn-primary btn-lg" href="#">Marque uma Consulta</a></p>
                           </div>
                       </div>
                   </div>
               </div>
           </li>
           <li style="background-image: url(images/img_bg_2.jpg);">
               <div class="overlay-gradient"></div>
               <div class="container">
                   <div class="row">
                       <div class="col-md-8 col-md-offset-2 text-center js-fullheight slider-text">
                           <div class="slider-text-inner">
                               <h1>Consultoria Jurídica Inovadora</h1>
                                <h2>Um Novo Conceito de Direito</h2>
                                <p><a class="btn btn-primary btn-lg btn-learn" href="#">Marque uma Consulta</a></p>
                           </div>
                       </div>
                   </div>
               </div>
           </li>
           <li style="background-image: url(images/img_bg_3.jpg);">
               <div class="overlay-gradient"></div>
               <div class="container">
                   <div class="row">
                       <div class="col-md-8 col-md-offset-2 text-center js-fullheight slider-text">
                           <div class="slider-text-inner">
                               <h1>Defenda Seu Direito Constitucional Com Ajuda Legal</h1>
                                <h2>Marque uma Consulta aqui! <a href="#" target="_blank">consultas.drconsultoriajuridica.com</a></h2>
                                <p><a class="btn btn-primary btn-lg btn-learn" href="#">Marque uma Consulta</a></p>
                           </div>
                       </div>
                   </div>
               </div>
           </li>
          </ul>
      </div>
</aside>
<div id="intro-bg">
    <div class="container">
        <div id="colorlib-intro">
            <div class="third-col">
                <span class="icon"><i class="icon-cog"></i></span>
                <h2>Nossa Missão.</h2>
                <p>Nossa Missão é transformar seus problemas em soluções. Transformando fatos em Direito.</p>
                <h2>Nossa Visão.</h2>
                <p>Levar o Direito ao alcance de todos.</p>
            </div>
            <div class="third-col third-col-color">
                <span class="icon"><i class="icon-old-phone"></i></span>
                <h2>Ligue Agora:  (92) 98191-8315</h2>
                <p>Email: <a href="#"> atendimento@drconsultoria.com</a></p>
                <p>Ligue-nos ou envie um email, ficaremos felizes em lhe atender</p>
            </div>
        </div>
    </div>
</div>

<div id="colorlib-counter" class="colorlib-counters" style="background-image: url(images/img_bg_3.jpg);" data-stellar-background-ratio="0.5">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-3 text-center animate-box">
                <span class="icon"><i class="flaticon-lawyer-1"></i></span>
                <span class="colorlib-counter js-counter" data-from="0" data-to="1539" data-speed="5000" data-refresh-interval="50"></span>
                <span class="colorlib-counter-label">Advogados Experientes</span>
            </div>
            <div class="col-md-3 text-center animate-box">
                <span class="icon"><i class="flaticon-courthouse"></i></span>
                <span class="colorlib-counter js-counter" data-from="0" data-to="3653" data-speed="5000" data-refresh-interval="50"></span>
                <span class="colorlib-counter-label">Clientes Satisfeitos</span>
            </div>
            <div class="col-md-3 text-center animate-box">
                <span class="icon"><i class="flaticon-libra"></i></span>
                <span class="colorlib-counter js-counter" data-from="0" data-to="5987" data-speed="5000" data-refresh-interval="50"></span>
                <span class="colorlib-counter-label">Casos de Sucesso</span>
            </div>
            <div class="col-md-3 text-center animate-box">
                <span class="icon"><i class="flaticon-police-badge"></i></span>
                <span class="colorlib-counter js-counter" data-from="0" data-to="3999" data-speed="5000" data-refresh-interval="50"></span>
                <span class="colorlib-counter-label">Prêmios e Honras</span>
            </div>
        </div>
    </div>
</div>

<div id="colorlib-content">
    <div class="video colorlib-video" style="background-image: url(images/video.jpg);">
        <a href="https://www.youtube.com/watch?v=gGTgnnNDkOA" class="popup-vimeo"><i class="icon-video2"></i></a>
        <div class="overlay"></div>
    </div>
    <div class="choose animate-box">
        <div class="colorlib-heading">
            <h2>30 Anos de experiências, executando serviços de alta qualidade</h2>
            <p>Não se deve ter vergonha de reconhecer um erro, pois assim se demonstra ser mais sábio hoje do que ontem. (Jonathan Swift) </p>
        </div>
        <div class="progress">
            <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width:75%">
            Direito de Sucessão 75%
            </div>
        </div>
        <div class="progress">
            <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100" style="width:90%">
            Direito de Familia 90%
            </div>
        </div>
        <div class="progress">
            <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:70%">
            Direito Empresárial 70%
            </div>
        </div>
        <div class="progress">
            <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="86" aria-valuemin="0" aria-valuemax="100" style="width:86%">
            Danos Morais 86%
            </div>
        </div>
    </div>
</div>

<div id="colorlib-practice">
    <div class="container">
        <div class="row animate-box">
            <div class="col-md-8 col-md-offset-2 text-center colorlib-heading">
                <h2>Área de Atuação</h2>
                <p>Somos especializados em Direito Empresarial (EPP, EIRELI, ME, LTDA), Direitos Humanos, Direito Privado, Direito Indenizatório (Danos Morais e Materiais), Direito de Família, Direito Sucessão, Direito das Coisas</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 text-center animate-box">
                <div class="services">
                    <span class="icon">
                        <i class="flaticon-courthouse"></i>
                    </span>
                    <div class="desc">
                        <h3><a href="#">Direito Empresarial</a></h3>
                        <p>O Direito imobiliário busca regular uma série de relações particulares, como a posse, as mais variadas formas de aquisição ou perda da propriedade.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 text-center animate-box">
                <div class="services">
                    <span class="icon">
                        <i class="flaticon-jury"></i>
                    </span>
                    <div class="desc">
                        <h3><a href="#">Direito de Família</a></h3>
                        <p>Direito de família é o ramo do direito que contém normas jurídicas relacionadas com a estrutura, organização e proteção da família.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 text-center animate-box">
                <div class="services">
                    <span class="icon">
                        <i class="flaticon-folder"></i>
                    </span>
                    <div class="desc">
                        <h3><a href="#">Direito Empresarial</a></h3>
                        <p>Direito comercial ou direito empresarial e o conjunto de normas disciplinadoras da atividade negocial do empresário, e de qualquer pessoa física ou jurídica, destinada a fins de natureza econômica.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 text-center animate-box">
                <div class="services">
                    <span class="icon">
                        <i class="flaticon-policeman"></i>
                    </span>
                    <div class="desc">
                            <h3><a href="#">Danos Morais</a></h3>
                            <p>Danos morais são as perdas sofridas por um ataque à moral e à dignidade das pessoas, caracterizados como uma ofensa à reputação da vítima. Qualquer perda que abale à honra pode ser caracterizada como dano moral.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 text-center animate-box">
                <div class="services">
                    <span class="icon">
                        <i class="flaticon-handcuffs"></i>
                    </span>
                    <div class="desc">
                        <h3><a href="#">Negligencia Médica</a></h3>
                        <p>O médico negligente é aquele profissional que age de forma omissa, com total descaso de seus deveres éticos com o paciente.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 text-center animate-box">
                <div class="services">
                    <span class="icon">
                        <i class="flaticon-libra"></i>
                    </span>
                    <div class="desc">
                        <h3><a href="#">Direito Penal</a></h3>
                        <p>O direito penal ou direito criminal é a disciplina de direito público que regula o exercício do poder punitivo do Estado</p>
                    </div>
                </div>
            </div>
            {{--  <div class="col-md-12 text-center animate-box">
                <p><a class="btn btn-primary btn-lg btn-learn" href="#">View More <i class="icon-arrow-right"></i></a></p>
            </div>  --}}

        </div>
    </div>
</div>

<div id="colorlib-started" style="background-image:url(images/img_bg_2.jpg);" data-stellar-background-ratio="0.5">
    <div class="overlay"></div>
    <div class="container">
        <div class="row animate-box">
            <div class="col-md-8 col-md-offset-2 text-center colorlib-heading colorlib-heading2">
                <h2>30 Anos de experiência em vários casos</h2>
                <p>Ajudamos as pessoas a combater eficazmente seus infratores e defender seu direito com sucesso!</p>
                <p><a href="#" class="btn btn-primary btn-lg">Consulta</a></p>
            </div>
        </div>
    </div>
</div>


<div id="colorlib-testimonial" class="colorlib-bg-section">
    <div class="container">
        <div class="row animate-box">
            <div class="col-md-6 col-md-offset-3 text-center colorlib-heading">
                <h2>O que dizem os Clientes</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="row animate-box">
                    <div class="owl-carousel owl-carousel-fullwidth">
                        <div class="item">
                            <div class="testimony-slide active text-center">
                                <figure>
                                    <img src="images/user-1.jpg" alt="user">
                                </figure>
                                <span>Jean Doe, via <a href="#" class="twitter">Twitter</a></span>
                                <blockquote>
                                    <p>&ldquo;Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.&rdquo;</p>
                                </blockquote>
                            </div>
                        </div>
                        <div class="item">
                            <div class="testimony-slide active text-center">
                                <figure>
                                    <img src="images/user-1.jpg" alt="user">
                                </figure>
                                <span>John Doe, via <a href="#" class="twitter">Twitter</a></span>
                                <blockquote>
                                    <p>&ldquo;Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.&rdquo;</p>
                                </blockquote>
                            </div>
                        </div>
                        <div class="item">
                            <div class="testimony-slide active text-center">
                                <figure>
                                    <img src="images/user-1.jpg" alt="user">
                                </figure>
                                <span>John Doe, via <a href="#" class="twitter">Twitter</a></span>
                                <blockquote>
                                    <p>&ldquo;Far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.&rdquo;</p>
                                </blockquote>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="colorlib-consult">
    <div class="video colorlib-video" style="background-image: url(images/video.jpg);" data-stellar-background-ratio="0.5">
    </div>
    <div class="choose choose-form animate-box">
        <div class="colorlib-heading">
            <h2>Tenha uma Consulta Gratuita</h2>
        </div>


        {!! Form::open(['route' => 'sendConsulta', 'method' => 'post']) !!}

            <div class="row form-group">
                <div class="col-md-12">
                    <!-- <label for="name">Nome</label> -->
                    <input type="text" name="name" class="form-control" placeholder="Nome Completo">
                    @if ($errors->has('name'))
                        <small class="form-text invalid-feedback"> {{ $errors->first('name') }}</small>
                    @endif
                </div>
            </div>

            <div class="row form-group">
                <div class="col-md-12">
                    <!-- <label for="email">Email</label> -->
                    <input type="email" name="email" id="email" class="form-control" placeholder="E-mail">
                    @if ($errors->has('email'))
                        <small class="form-text invalid-feedback"> {{ $errors->first('email') }}</small>
                    @endif
                </div>
            </div>

            <div class="row form-group">
                <div class="col-md-12">
                    <!-- <label for="subject">Assunto</label> -->
                    <input type="text" name="subject" class="form-control" placeholder="Assunto da Consulta">
                    @if ($errors->has('subject'))
                        <small class="form-text invalid-feedback "> {{ $errors->first('subject') }}</small>
                    @endif
                </div>
            </div>

            <div class="row form-group">
                <div class="col-md-12">
                    <!-- <label for="message">Mensagem</label> -->
                    <textarea name="message" id="message" cols="30" rows="10" class="form-control" placeholder="Faça aqui sua pergunta"></textarea>
                    @if ($errors->has('message'))
                        <small class="form-text invalid-feedback"> {{ $errors->first('message') }}</small>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <input type="submit" value="Enviar Consulta" class="btn btn-primary">
            </div>
        {!! Form::close() !!}
    </div>
</div>

{{--
<div id="colorlib-blog">
    <div class="container">
        <div class="row animate-box">
            <div class="col-md-8 col-md-offset-2 text-center colorlib-heading">
                <h2>Recent Post</h2>
                <p>Dignissimos asperiores vitae velit veniam totam fuga molestias accusamus alias autem provident. Odit ab aliquam dolor eius.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="blog-featured animate-box">
                    <a href="blog.html"><img class="img-responsive" src="images/blog-1.jpg" alt=""></a>
                    <h2><a href="blog.html">Child Abuse Cases Are Our First Priority</a></h2>
                    <p class="meta"><span>Jan 5, 2017</span> | <span>3 Comments</span></p>
                    <p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however</p>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12 animate-box">
                        <div class="blog-entry">
                            <a href="blog.html" class="thumb"><img class="img-responsive" src="images/blog-2.jpg" alt=""></a>
                            <div class="desc">
                                <h3><a href="blog.html">Family Law Is Now On Court</a></h3>
                                <p class="meta"><span>Jan 5, 2017</span> | <span>3 Comments</span></p>
                                <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 animate-box">
                        <div class="blog-entry">
                            <a href="blog.html" class="thumb"><img class="img-responsive" src="images/blog-3.jpg" alt=""></a>
                            <div class="desc">
                                <h3><a href="blog.html">Family Law Is Now On Court</a></h3>
                                <p class="meta"><span>Jan 5, 2017</span> | <span>3 Comments</span></p>
                                <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 animate-box">
                        <div class="blog-entry">
                            <a href="blog.html" class="thumb"><img class="img-responsive" src="images/blog-1.jpg" alt=""></a>
                            <div class="desc">
                                <h3><a href="blog.html">Family Law Is Now On Court</a></h3>
                                <p class="meta"><span>Jan 5, 2017</span> | <span>3 Comments</span></p>
                                <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
--}}
<div id="colorlib-about">
    <div class="container">
        <div class="row animate-box">
            <div class="col-md-8 col-md-offset-2 text-center colorlib-heading">
                <h2>Nossos Advogados</h2>
                <p>Dignissimos asperiores vitae velit veniam totam fuga molestias accusamus alias autem provident. Odit ab aliquam dolor eius.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 col-sm-3 text-center animate-box" data-animate-effect="fadeIn">
                <div class="colorlib-staff">
                    <img src="images/user-2.jpg" alt="Template">
                    <h3>John Simon</h3>
                    <strong class="role">Counsel</strong>
                    <p>Quos quia provident consequuntur culpa facere ratione maxime commodi voluptates id repellat velit eaque aspernatur expedita. Possimus itaque adipisci.</p>
                    <ul class="colorlib-social-icons">
                        <li><a href="#"><i class="icon-facebook"></i></a></li>
                        <li><a href="#"><i class="icon-twitter"></i></a></li>
                        <li><a href="#"><i class="icon-dribbble"></i></a></li>
                        <li><a href="#"><i class="icon-github"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 text-center animate-box" data-animate-effect="fadeIn">
                <div class="colorlib-staff">
                    <img src="images/user-2.jpg" alt="Template">
                    <h3>John Doe</h3>
                    <strong class="role">Head of International Practice</strong>
                    <p>Quos quia provident consequuntur culpa facere ratione maxime commodi voluptates id repellat velit eaque aspernatur expedita. Possimus itaque adipisci.</p>
                    <ul class="colorlib-social-icons">
                        <li><a href="#"><i class="icon-facebook"></i></a></li>
                        <li><a href="#"><i class="icon-twitter"></i></a></li>
                        <li><a href="#"><i class="icon-dribbble"></i></a></li>
                        <li><a href="#"><i class="icon-github"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 text-center animate-box" data-animate-effect="fadeIn">
                <div class="colorlib-staff">
                    <img src="images/user-2.jpg" alt="Template">
                    <h3>Peter Washington</h3>
                    <strong class="role">Managing Partner, Attorney</strong>
                    <p>Quos quia provident consequuntur culpa facere ratione maxime commodi voluptates id repellat velit eaque aspernatur expedita. Possimus itaque adipisci.</p>
                    <ul class="colorlib-social-icons">
                        <li><a href="#"><i class="icon-facebook"></i></a></li>
                        <li><a href="#"><i class="icon-twitter"></i></a></li>
                        <li><a href="#"><i class="icon-dribbble"></i></a></li>
                        <li><a href="#"><i class="icon-github"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 text-center animate-box" data-animate-effect="fadeIn">
                <div class="colorlib-staff">
                    <img src="images/user-2.jpg" alt="Template">
                    <h3>Peter Washington</h3>
                    <strong class="role">Managing Partner, Attorney</strong>
                    <p>Quos quia provident consequuntur culpa facere ratione maxime commodi voluptates id repellat velit eaque aspernatur expedita. Possimus itaque adipisci.</p>
                    <ul class="colorlib-social-icons">
                        <li><a href="#"><i class="icon-facebook"></i></a></li>
                        <li><a href="#"><i class="icon-twitter"></i></a></li>
                        <li><a href="#"><i class="icon-dribbble"></i></a></li>
                        <li><a href="#"><i class="icon-github"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
